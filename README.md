# WedUpp Test (Frontend Application)

This application is build in VueJs. 

This app has form which takes Input from user. On submitting the form, the app validates the input. If validated successfully, then it displays the words.

It performs following tasks:

1. Displays the form
2. On submitting the form, validates the input
3. If validated successfully, make an AJAX call to server
4. On receiving successful output, displays results to user.


Libraries used:

1. Axios - To make POST call to backend
2. Bootstrap - For UI
